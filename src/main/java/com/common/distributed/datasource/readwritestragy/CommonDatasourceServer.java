/**
 * 
 */
package com.common.distributed.datasource.readwritestragy;

import java.io.Serializable;

import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;

/**
 * @author liubing1
 *
 */
public class CommonDatasourceServer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7040426931464129112L;

	private CommonDatasourceSchema commonDatasourceSchema;
	
	public int weight;
    
	public int effectiveWeight;
    
	public int currentWeight;
	
	private boolean down=false;
	/**
	 * @return the commonDatasourceSchema
	 */
	public CommonDatasourceSchema getCommonDatasourceSchema() {
		return commonDatasourceSchema;
	}

	/**
	 * @param commonDatasourceSchema the commonDatasourceSchema to set
	 */
	public void setCommonDatasourceSchema(
			CommonDatasourceSchema commonDatasourceSchema) {
		this.commonDatasourceSchema = commonDatasourceSchema;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
		if(weight==0){
			setDown(true);
		}else{
			setDown(false);
		}
		
	}

	/**
	 * @return the effectiveWeight
	 */
	public int getEffectiveWeight() {
		return effectiveWeight;
	}

	/**
	 * @param effectiveWeight the effectiveWeight to set
	 */
	public void setEffectiveWeight(int effectiveWeight) {
		this.effectiveWeight = effectiveWeight;
	}

	/**
	 * @return the currentWeight
	 */
	public int getCurrentWeight() {
		return currentWeight;
	}

	/**
	 * @param currentWeight the currentWeight to set
	 */
	public void setCurrentWeight(int currentWeight) {
		this.currentWeight = currentWeight;
	}

	public CommonDatasourceServer() {

	}

	/**
	 * @return the down
	 */
	public boolean isDown() {
		return down;
	}

	/**
	 * @param down the down to set
	 */
	public void setDown(boolean down) {
		this.down = down;
	}
    
	
}
