/**
 * 
 */
package com.common.distributed.datasource.enums;

/**
 * @author liubing1
 *  数据源类型
 */
public enum CommonDatasourceType {

	 WRITE,
	 READ;
}
