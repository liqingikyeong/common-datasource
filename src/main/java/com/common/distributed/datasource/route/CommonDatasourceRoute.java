/**
 * 
 */
package com.common.distributed.datasource.route;

import java.util.HashMap;
import java.util.Map;

import com.common.distributed.datasource.CommonDatasource;
import com.common.distributed.datasource.core.shard.support.TableBean;
import com.common.distributed.datasource.enums.RouteContextEnum;
import com.common.distributed.datasource.exception.CommonDatasourceException;
import com.common.distributed.datasource.route.support.TableNameBean;
import com.common.distributed.datasource.spring.config.schema.CommonDatasourceSchema;
import com.common.distributed.datasource.spring.config.schema.DataBaseSchema;
import com.common.distributed.datasource.spring.datasource.CustomerContextHolder;
import com.common.distributed.datasource.util.BeanPropertyAccessUtil;
import com.common.distributed.datasource.util.StringUtil;

/**
 * @author liubing1 
 * 分布式路由功能，改变参数结构
 */
public class CommonDatasourceRoute {

	private CommonDatasourceRoute() {

	}

	private static class SingletonHolder {
		static final CommonDatasourceRoute instance = new CommonDatasourceRoute();
	}

	public static CommonDatasourceRoute getInstance() {
		return SingletonHolder.instance;
	}
	/**
	 * 根据参数路由
	 * @param commonDatasourceSchema
	 * @param parameterObj
	 * @return
	 */
	public Object doDataSourceRoute(
			CommonDatasourceSchema commonDatasourceSchema, Object parameterObj) {
		// TODO Auto-generated method stub
		 
       try {
        		 Map parametersMap =new HashMap();
        		 if (parameterObj instanceof Map) {// map 
        			  parametersMap = (Map) parameterObj;
        		 }else{//java bean
        			 parametersMap=BeanPropertyAccessUtil.transBean2Map(parameterObj);
        		 }
        		TableNameBean tableNameBean=getTableFiedlName(commonDatasourceSchema, parametersMap);
        		TableBean tableBean=getTableBean(commonDatasourceSchema, tableNameBean);
        		BeanPropertyAccessUtil.rewriteTableName(parameterObj, RouteContextEnum.tablename.toString(), tableBean.getPrefixname());
				return parameterObj;
		} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new CommonDatasourceException("参数必须是map 或者javabean",e); 
		}  
         
	}
	/**
	 * 获取分库分表的字段名
	 * @param commonDatasourceSchema
	 * @param parametersMap
	 * @return
	 */
	private TableNameBean getTableFiedlName(CommonDatasourceSchema commonDatasourceSchema,Map parametersMap){
		TableNameBean tableNameBean=new TableNameBean();
		 for(String tablefield:commonDatasourceSchema.getTablenameFields()){
			 if(parametersMap.containsKey(tablefield)){
				  tableNameBean.setTablefieldvalue(String.valueOf(parametersMap.get(tablefield)));
				  tableNameBean.setTablefield(tablefield);
				  break;
			 }
		 }
		 if(StringUtil.isNullOrEmpty(tableNameBean.getTablefield())){
			 throw new CommonDatasourceException("参数必须含有分库分表字段");
		 }
		 return tableNameBean;
	}
	/**
	 * 获取表名
	 * @param commonDatasourceSchema
	 * @param tablenamefield
	 * @return
	 */
	private TableBean getTableBean(CommonDatasourceSchema commonDatasourceSchema,TableNameBean tableNameBean){
		DataBaseSchema dataBaseSchema=CommonDatasource.getInstance().getShardDataSoure(commonDatasourceSchema, tableNameBean);
		CustomerContextHolder.setCustomerType(dataBaseSchema);//设置数据源
		 
		TableBean tableBean=CommonDatasource.getInstance().getShardTableName(commonDatasourceSchema, tableNameBean, dataBaseSchema.getDatabaseName());
		return tableBean; 
	}
}
