/**
 * 
 */
package com.common.distributed.datasource.route.support;

import java.io.Serializable;

/**
 * @author liubing1
 *
 */
public class TableNameBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1588694988612959719L;
	
	private String tablefield;
	
	private String tablefieldvalue;

	/**
	 * @return the tablefield
	 */
	public String getTablefield() {
		return tablefield;
	}

	/**
	 * @param tablefield the tablefield to set
	 */
	public void setTablefield(String tablefield) {
		this.tablefield = tablefield;
	}

	/**
	 * @return the tablefieldvalue
	 */
	public String getTablefieldvalue() {
		return tablefieldvalue;
	}

	/**
	 * @param tablefieldvalue the tablefieldvalue to set
	 */
	public void setTablefieldvalue(String tablefieldvalue) {
		this.tablefieldvalue = tablefieldvalue;
	}

	public TableNameBean() {
		super();
	}
	
	
}
